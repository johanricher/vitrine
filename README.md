# Vitrine

_Web site displaying pages & data retrieved dynamically from other sites_

![Screenshot of a web site based on Vitrine](https://forum.parlement-ouvert.fr/uploads/default/optimized/1X/a0a1d5850054eadca16d05a019b5165e08ed9c87_1_623x500.jpg)

_Vitrine_ is an application that generates dynamically the landing pages of a web site using documents & data retrieved from other sites (using their APIs).

It can currently use pages from:
* [Discourse](https://www.discourse.org/) forums
* [GitLab](https://about.gitlab.com/) projects & groups

It can be used as the home page for a hackathon, to display the projects, the datasets, the participants, etc.

_Vitrine_ started as a clone of [Hackdash](https://hackdash.org/), but instead of having its own back-office, it used Discourse and benefits from it power and versatility.

Usages examples:

* [#dataFin hackathon](https://datafin.fr/)
* [bureau ouvert de la députée Paula Forteza](https://parlement-ouvert.fr/)

_Vitrine_ is free and open source software.

* [software repository](https://framagit.org/eraviart/vitrine)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/eraviart/vitrine/blob/master/LICENSE.md)

## Installation & Configuration

```bash
git clone https://framagit.org/eraviart/vitrine.git
cd vitrine/
npm install
```

## Customization

To customize Vitrine, edit `site/index.js`. Then validate it and generate `site/site.json` by running:
```bash
node generate-site.js site/
```

### Development

```bash
npm run dev
```

### Development

```bash
npm run build
```

## Discourse Configuration

Discourse must be configurated to accept requests coming from another site (ie CORS must be enabled to accept requests coming from Vitrine web site).

In file `/var/discourse/containers/app.yml`, add line:
```yaml
  env:
    [...]
    DISCOURSE_ENABLE_CORS: true
```

Then in Discourse admin web page `/admin/site_settings/category/security` fill field **cors origin** with the URL of the Vitrine web site.
