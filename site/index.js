export default {
  apiUrl: "https://forum.datafin.fr/",
  banner: {
    backgroundImageUrl:
      "https://forum.datafin.fr/uploads/default/original/1X/4e04278e924a63144461036fe45ceb87c29a826a.png",
    buttonTitle: "Consultez l'enquête de satisfaction",
    buttonUrl: "/enquete-sur-le-hackathon-datafin-de-juin-2018",
    logoUrl: "https://forum.datafin.fr/uploads/default/original/1X/0ccec392b57b81b41b235b327dd6d2132b292377.png",
    title: `\
<h3><small>Exploitez les données financières publiques !</small></h3>
<h4>Après le hackathon des 15 et 16 juin 2018, à l'Assemblée nationale...</h4>
`,
  },
  description:
    "Site collaboratif du hackathon #dataFin consacré aux données financières publiques, les 15 et 16 juin 2018, à l’Assemblée nationale",
  language: "fr",
  pages: [
    {
      type: "topic",
      id: 8,
      slug: "bienvenue-au-hackathon-datafin",
      title: "Présentation",
    },
    {
      type: "category",
      id: 6,
      showId: true,
      newTopicBody: `\
![Logo du défi](/uploads/default/original/1X/4ef7f1da35933bc31fd11b2d94b7336c0789a557.png)

DESCRIPTION_DEFI

* [Site](LIEN_VERS_SITE)
* [Dépôt du code source](LIEN_VERS_DEPOT)
* Licence du logiciel : [NOM_LICENCE](LIEN_VERS_LICENCE)

## Jeux de données utilisés

https://www.data.gouv.fr/fr/datasets/le-budget-analyse-et-evolution-00000000/

https://www.data.gouv.fr/fr/datasets/ID_JEU_DE_DONNEES/

## Besoins

* data scientists
* designers
* développeurs
* économistes
* ergonomes
* graphistes
* journalistes
`,
      newTopicButtonTitle: "Proposer un nouveau défi",
      slug: "defis",
      title: "Défis",
      titleLong: "Défis du hackathon",
    },
    {
      type: "category",
      id: 5,
      newTopicBody: `\
![Logo du jeu de données](/uploads/default/original/1X/4ef7f1da35933bc31fd11b2d94b7336c0789a557.png)

DESCRIPTION

* [Jeu de données](LIEN_VERS_SITE)
* Licence des données : [NOM_LICENCE](LIEN_VERS_LICENCE)
`,
      newTopicButtonTitle: "Ajouter un jeu de données",
      slug: "jeux-de-donnees",
      title: "Données",
      titleLong: "Jeux de données open data pouvant servir au hackathon",
    },
    {
      type: "category",
      id: 10,
      newTopicBody: "",
      newTopicButtonTitle: "Ajouter un outil",
      slug: "outils",
      title: "Outils",
      titleLong: "Outils libres et ouverts pouvant servir au hackathon",
    },
    // {
    //   type: "category",
    //   id: 8,
    //   newTopicBody: "",
    //   newTopicButtonTitle: "Ajouter un profil",
    //   slug: "vous-etes",
    //   title: "Vous êtes…",
    // },
    {
      type: "topic",
      id: 300,
      slug: "enquete-sur-le-hackathon-datafin-de-juin-2018",
      title: "Enquête",
    },
    {
      type: "topic",
      id: 47,
      slug: "salon-de-discussion-chat-du-hackathon",
      title: "Chat",
    },
    // {
    //   type: "internal_link",
    //   slug: "live",
    //   title: "Live",
    // },
    {
      type: "topic",
      id: 114,
      slug: "galerie-de-photos-du-precedent-hackathon",
      title: "Photos",
    },
    // {
    //   type: "category",
    //   id: 11,
    //   newTopicBody: "",
    //   newTopicButtonTitle: "Ajouter un article de presse",
    //   slug: "presse",
    //   title: "Presse",
    //   titleLong: "La presse en parle",
    // },
    {
      type: "topic",
      id: 31,
      slug: "faq-foire-aux-questions-du-hackathon",
      title: "FAQ",
    },
    // {
    //   type: "topic",
    //   id: 24,
    //   slug: "a-propos",
    //   title: "À propos",
    // },
  ],
  title: "#dataFin",
  twitterName: "#dataFin",
  zones: [
    {
      type: "category",
      id: 6,
      showId: true,
      limit: 4,
      pageButtonTitle: "Voir tous les défis",
      slug: "defis",
      title: "Les défis à la une",
    },
    {
      type: "category",
      id: 5,
      limit: 4,
      pageButtonTitle: "Voir tous les jeux de données",
      slug: "jeux-de-donnees",
      title: "Les jeux de données proposés",
    },
    {
      type: "category",
      id: 10,
      limit: 4,
      pageButtonTitle: "Voir tous les outils",
      slug: "outils",
      title: "Des outils pouvant servir",
    },
    {
      type: "category",
      id: 8,
      limit: 4,
      pageButtonTitle: "Voir tous les profils",
      slug: "vous-etes",
      title: "Vous êtes…",
    },
    {
      type: "category",
      id: 9,
      limit: 4,
      slug: "partenaires",
      title: "Les partenaires",
    },
  ],
}
