// Server functions to generate sites & surveys

import fs from "fs"
import marked from "marked"
import path from "path"
import resolveUrl from "url-resolve"

const renderer = new marked.Renderer()
const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i
renderer.image = function(href, title, text) {
  if (this.options.baseUrl && !originIndependentUrl.test(href)) {
    href = resolveUrl(this.options.baseUrl, href)
  }
  text = text ? ` alt="${text}"` : ""
  title = title ? ` title="${title}"` : ""
  const slash = this.options.xhtml ? "/" : ""
  return `<img${text} class="img-fluid" src="${href}"${title}${slash}>`
}
const markedOptions = {
  renderer,
}

export function generateSite(siteDir) {
  if (!fs.existsSync(siteDir)) {
    return [null, `Directory "${siteDir}" doesn't exist`]
  }
  if (!fs.lstatSync(siteDir).isDirectory()) {
    return [null, `Node "${siteDir}" isn't a directory`]
  }
  const siteFilePath = path.join(siteDir, "index.js")
  if (!fs.existsSync(siteFilePath)) {
    return [null, `File "${siteFilePath}" doesn't exist`]
  }
  if (!fs.lstatSync(siteFilePath).isFile()) {
    return [null, `Node "${siteFilePath}" isn't a file`]
  }
  const { default: site } = require(path.join("../", siteFilePath))

  site.dir = siteDir

  // const errors = {}

  const generatedSiteFilePath = path.join(siteDir, "site.json")
  fs.writeFileSync(generatedSiteFilePath, JSON.stringify(site, null, 2))

  return [site, null]
}

function markdownToHtml(text, options) {
  options = {
    ...markedOptions,
    ...(options || {}),
  }
  text = text.replace(/src="(.*?)"/g, (match, url) => {
    if (options.baseUrl && !originIndependentUrl.test(url)) {
      url = resolveUrl(options.baseUrl, url)
    }
    return `src="${url}"`
  })
  return marked(text, options)
}
