import compression from "compression"
import fs from "fs"
import path from "path"
import polka from "polka"
import sirv from "sirv"
import { Store } from "svelte/store.js"

import * as sapper from "../__sapper__/server.js"
import config from "./config"
import "./global.scss"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"
const siteDir = path.resolve(config.siteDir)
const siteText = fs.readFileSync(path.join(siteDir, "site.json"))
const site = JSON.parse(siteText)

polka() // You can also use Express
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      store: (/* req */) => {
        return new Store({ site })
      },
    })
  )
  .listen(PORT, error => {
    if (error) {
      console.log(`Error when calling listen on port ${PORT}:`, error)
    }
  })
