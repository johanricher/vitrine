import commandLineArgs from "command-line-args"

import { generateSite } from "./generators"

const optionsDefinition = [{ alias: "s", defaultOption: true, name: "site-dir", type: String }]
const options = commandLineArgs(optionsDefinition)
const siteDir = options["site-dir"]
// eslint-disable-next-line no-unused-vars
const [site, error] = generateSite(siteDir)
if (error !== null) {
  console.error(JSON.stringify(error, null, 2))
  process.exit(1)
}
process.exit(0)
