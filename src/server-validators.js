// Server-side validators

import fs from "fs"
import path from "path"

export function validateConfig(config) {
  if (config === null || config === undefined) {
    return [config, "Missing config"]
  }
  if (typeof config !== "object") {
    return [config, `Expected an object got "${typeof config}"`]
  }

  config = { ...config }
  const errors = {}
  const remainingKeys = new Set(Object.keys(config))

  {
    const key = "siteDir"
    if (remainingKeys.delete(key)) {
      let dir = config[key]
      if (typeof dir !== "string") {
        errors[key] = `Expected a string got "${typeof dir}"`
      } else {
        dir = path.normalize(path.join("src/", dir.trim()))
        const absoluteDir = path.resolve(dir)
        if (!fs.existsSync(dir)) {
          errors[key] = `Directory "${absoluteDir}" doesn't exist`
        } else {
          if (!fs.lstatSync(dir).isDirectory()) {
            errors[key] = `Node "${absoluteDir}" isn't a directory`
          } else {
            config[key] = dir
          }
        }
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [config, Object.keys(errors).length === 0 ? null : errors]
}
